\section{**OPENRSP}\label{sec:openrsp}
This section describes the keywords needed for obtaining molecular properties
using OpenRSP library, at the HF and DFT levels based on the atomic orbital
based response formulation~\cite{thorvaldsen:214108}.

The section begins with **OPENRSP, and for the time being contains information
of two different but related tasks: response function and residue calculations.
The general structure is thus:

\begin{description}
  \item[\Keyinfo{**OPENRSP}]
  \item[\Keyinfo{*RESPONSE FUNCTION}]
  \item[\Keyinfo{\$PROPERTY}]
  \item[\Keyinfo{.Specific (optional) information for property 1}]
  \item[\Keyinfo{\$PROPERTY}]
  \item[\Keyinfo{.Specific (optional) information for property 2}]
  \item[\Keyinfo{...}]
  \item[\Keyinfo{*RESIDUE}]
  \item[\Keyinfo{.General information for residues}]
  \item[\Keyinfo{\$PROPERTY}]
  \item[\Keyinfo{.Specific (optional) information for property 1}]
  \item[\Keyinfo{\$PROPERTY}]
  \item[\Keyinfo{.Specific (optional) information for property 2}]
  \item[\Keyinfo{...}]
\end{description}

The test cases can be found in the \textbf{LSDALTON/test/openrsp} directory.
To run a test case, go to the \textbf{test/} directory and type:
\begin{description}
  \item \verb|./TEST <test case>|
\end{description}
The test case files contains simple examples of input files.

In the following, we give the list of keywords for the response function and
residue calculations.

\subsection{*RESPONSE FUNCTION}\label{subsec:openrsp-rspfun}

The molecular response functions can be written as:
\begin{align}
  \langle\langle\hat{A};\hat{B}_{1},\cdots,\hat{B}_{n} %
    \rangle\rangle_{\omega_{B_{1}},\cdots,\omega_{B_{n}}}
  \label{eq-openrsp-rspfun}
  &=\left.\frac{{\rm d}^{n+1}\left\{L(t)\right\}_{T}} %
    {{\rm d}\varepsilon_{\omega_{A}} %
    {\rm d}\varepsilon_{\omega_{B_{1}}}\cdots %
    {\rm d}\varepsilon_{\omega_{B_{n}}}} %
    \right|_{\{\varepsilon\}=0},\\
  \label{eq-openrsp-a-freq}
  \omega_{A}&=-\sum_{j=1}^{n}\omega_{B_{j}},
\end{align}
where $\left\{L(t)\right\}_{T}$ is the so-called time-averaged quasi-energy
Lagrangian, and the zero perturbation strength~($\{\varepsilon\}=0$) condition
is
$\varepsilon_{\omega_{A}},\varepsilon_{\omega_{B_{1}}},\cdots,\varepsilon_{\omega_{B_{n}}}=0$.

OpenRSP library can compute similar properties at one call to reuse some common
intermediate results, for instance, the polarizability and
hyper-polarizabilities can in principle be calculated at one call. Each
property is grouped into \Keyinfo{\$PROPERTY}, which contains the following
keywords:
\begin{description}
  \item[\Key{PERTURBATION}]~\newline
    \verb|<Number of perturbations>|\newline
    \verb|<Label of perturbation 1>|\newline
    \verb|<Label of perturbation 2>|\newline
    \verb|<...>|\newline
    Specifies the number of perturbations $n$ and their labels in
    Eq.~(\ref{eq-openrsp-rspfun}). Supported labels are:
    \begin{enumerate}
      \item \verb|EL| (electric perturbation).
      \item \verb|GEO| (geometric perturbation).
    \end{enumerate}
  \item[\Key{FREQUENCY}]~\newline
    \verb|<Number of frequency configurations>|\newline
    \verb|<Frequency of perturbation 2 of configuration 1>|\newline
    \verb|<Frequency of perturbation 3 of configuration 1>|\newline
    \verb|<...>|\newline
    \verb|<Frequency of perturbation 2 of configuration 2>|\newline
    \verb|<Frequency of perturbation 3 of configuration 2>|\newline
    \verb|<...>|\newline
    Specifies frequencies of perturbations $\hat{B}_{1},\cdots,\hat{B}_{n}$,
    and $\omega_{A}$ will be determined by Eq.~(\ref{eq-openrsp-a-freq}). Note
    that no frequency should be given for perturbation \verb|GEO|, which is
    always zero.
  \item[\Key{KN-RULE}]~\newline
    \verb|<Number k of (k,n) rule>|\newline
    Specifies number $k$ for the $(k, n)$ rule, where
    $0\le k\le\left\lfloor\verb|<Number of perturbations>|/2\right\rfloor$,
    and OpenRSP will determine the number $n$.
\end{description}

\subsection{*RESIDUE}\label{subsec:openrsp-residue}

The $m$-order residues of response functions are:
\begin{align}
  &\lim_{\omega_{B_{\pi_{m}}}\rightarrow\omega_{q_{m}}} %
    (\omega_{B_{\pi_{m}}}-\omega_{q_{m}}) %
  \biggl[\cdots\biggl[ %
  \lim_{\omega_{B_{\pi_{1}}}\rightarrow\omega_{q_{1}}} %
    (\omega_{B_{\pi_{1}}}-\omega_{q_{1}})\nonumber\\
  \label{eq-openrsp-residues}
  &\times\langle\langle\hat{A};\hat{B}_{\pi_{1}},\cdots,\hat{B}_{\pi_{m}}, %
    \hat{B}_{\pi_{m+1}}\rangle\rangle_{\omega_{B_{\pi_{1}}},\cdots, %
    \omega_{B_{\pi_{m}}},\omega_{B_{\pi_{m+1}}}}\biggr]\cdots\biggr],
\end{align}
where $\pi_{1},\cdots,\pi_{m},\pi_{m+1}$ are subsets of the set of perturbation
indices $\{1,\cdots,n\}$, and
\begin{equation}
  \label{eq-openrsp-residues-indices}
  \begin{cases}
    \pi_{i}\ne\emptyset,\quad 1\le i\le m,\\
    \pi_{i}\cap\pi_{j}=\emptyset,\quad 1\le i\ne j\le m+1,\\
    \cup_{i=1}^{m+1}\pi_{i}=\{1,\cdots,n\},
  \end{cases}
\end{equation}
and $\omega_{q_{j}}$~($1\le j\le m$) in the
residues~(\ref{eq-openrsp-residues}) is the frequency where excitation happens
from the ground state to an excited state $\left|q_{j}\right\rangle$.

For the time being, the excited states should be calculated by the following
keywords:
\begin{description}
  \item[\Keyinfo{**RESPONSE}]
  \item[\Keyinfo{.NEXCIT}]
  \item[\Keyinfo{<Number of excited states to be solved>}]
\end{description}
where the \Keyinfo{<Number of excited states to be solved>} should be adequate
for all $\left|q_{j}\right\rangle$~($1\le j\le m$) involved in the residues.

The order of residues $m$ and $q_{j}$~($1\le j\le m$) are respectively given by
the following two keywords:
\begin{description}
  \item[\Key{ORDER}]~\newline
    \verb|<Order of residues>|\newline
    Specifies the order of residues $m$ in Eq.~(\ref{eq-openrsp-residues}). For
    the time being, OpenRSP only supports single residue ($m=1$).
  \item[\Key{EXCITATION}]~\newline
    \verb|<Number of excitation tuples>|\newline
    \verb|<q_{1} q_{2} ... q_{m} of excitation tuple 1>|\newline
    \verb|<q_{1} q_{2} ... q_{m} of excitation tuple 2>|\newline
    \verb|<...>|\newline
    Specifies excitation tuples $q_{j}$~($1\le j\le m$) in
    Eq.~(\ref{eq-openrsp-residues}). For the time being, OpenRSP only supports
    1 excitation tuple.
\end{description}

Similar to the case of response functions, each property for residues is also
grouped into \Keyinfo{\$PROPERTY}, which contains the following keywords:
\begin{description}
  \item[\Key{PERTURBATION}]~\newline
    \verb|<Number of perturbations>|\newline
    \verb|<Label of perturbation 1>|\newline
    \verb|<Label of perturbation 2>|\newline
    \verb|<...>|\newline
    Specifies the number of perturbations $n$ and their labels in
    Eq.~(\ref{eq-openrsp-residues}). Supported labels are the same as those for
    response functions.
  \item[\Key{RESIDUE}]~\newline
    \verb|<Number of perturbation indices of subset 1>|\newline
    \verb|<Perturbation indices of subset 1>|\newline
    \verb|<...>|\newline
    \verb|<Number of perturbation indices of subset m>|\newline
    \verb|<Perturbation indices of subset m>|\newline
    Specifies subsets $\pi_{1},\cdots,\pi_{m}$ in
    Eq.~(\ref{eq-openrsp-residues}). For any subset $\pi_{i}$~($1\le i\le m$),
    if a negative number is given in
    \verb|<Number of perturbation indices of subset i>|, OpenRSP will take the
    limitation as $\lim_{\omega_{B_{\pi_{i}}}\rightarrow-\omega_{q_{i}}}$ in
    Eq.~(\ref{eq-openrsp-residues}).
  \item[\Key{FREQUENCY}]~\newline
    \verb|<Number of frequency configurations>|\newline
    \verb|<Frequency of perturbation 2 of configuration 1>|\newline
    \verb|<Frequency of perturbation 3 of configuration 1>|\newline
    \verb|<...>|\newline
    \verb|<Frequency of perturbation 2 of configuration 2>|\newline
    \verb|<Frequency of perturbation 3 of configuration 2>|\newline
    \verb|<...>|\newline
    Specifies frequencies of perturbations $\hat{B}_{1},\cdots,\hat{B}_{n}$,
    and $\omega_{A}$ will be determined by Eq.~(\ref{eq-openrsp-a-freq}). Note
    that no frequency should be given for perturbation \verb|GEO|, which is
    always zero.
  \item[\Key{KN-RULE}]~\newline
    \verb|<Number k of (k,n) rule>|\newline
    Specifies number $k$ for the $(k, n)$ rule, where
    $0\le k\le\left\lfloor\verb|<Number of perturbations>|/2\right\rfloor$,
    and OpenRSP will determine the number $n$.
\end{description}
