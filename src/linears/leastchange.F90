module leastchange_module
use files
use precision
use decompMod
use TYPEDEF, only: getNbasis
use TYPEDEFTYPE, only: lssetting, lsitem
use IntegralInterfaceMOD
use matrix_util
use matrix_module
use matrix_operations
use matrix_operations_aux
use LSparameters
use memory_handling
use TYPEDEF,only: count_ncore
contains

subroutine leastchange_to_oao_basis(decomp,A)
implicit none 
type(decompItem),intent(inout) :: decomp
type(Matrix) :: A, wrk

   call MAT_INIT(wrk,A%nrow,A%ncol)
   call mat_mul(decomp%U_inv,A,'n','n',1E0_realk,0E0_realk,wrk)
   call mat_mul(wrk,decomp%U_inv,'n','t',1E0_realk,0E0_realk,A)
   call mat_free(wrk)

end subroutine leastchange_to_oao_basis

subroutine leastchange_propint(decomp,ls,lupri,luerr,&
     & DIPX,DIPY,DIPZ,SECX,SECY,SECZ,n)
implicit none
integer      :: n, lupri,luerr
type(decompItem),intent(inout) :: decomp
integer, parameter  :: nderiv=2, nMAT=10
TYPE(lsitem) :: ls
type(matrix) :: DIPX,DIPY,DIPZ,SECX,SECY,SECZ
!local variables
INTEGER      :: i
 
I=2 !X
call mat_init(DIPX,n,n)
call II_get_single_carmom(lupri,luerr,ls%setting,DIPX,I,nderiv,0E0_realk,0E0_realk,0E0_realk)

I=3 !Y
call mat_init(DIPY,n,n)
call II_get_single_carmom(lupri,luerr,ls%setting,DIPY,I,nderiv,0E0_realk,0E0_realk,0E0_realk)

I=4 !Z
call mat_init(DIPZ,n,n)
call II_get_single_carmom(lupri,luerr,ls%setting,DIPZ,I,nderiv,0E0_realk,0E0_realk,0E0_realk)

I=5 !XX
call mat_init(SECX,n,n)
call II_get_single_carmom(lupri,luerr,ls%setting,SECX,I,nderiv,0E0_realk,0E0_realk,0E0_realk)

I=8 !YY
call mat_init(SECY,n,n)
call II_get_single_carmom(lupri,luerr,ls%setting,SECY,I,nderiv,0E0_realk,0E0_realk,0E0_realk)

I=10 !ZZ
call mat_init(SECZ,n,n)
call II_get_single_carmom(lupri,luerr,ls%setting,SECZ,I,nderiv,0E0_realk,0E0_realk,0E0_realk)

end subroutine leastchange_propint

subroutine leastchange_blocktransf(T,CMO,nbas,nocc,ncore)
implicit none
integer,intent(in)  :: nbas,ncore,nocc
type(matrix),intent(in)    :: CMO  !CMO(nbas,nbas)
type(matrix),intent(inout) :: T !T(nbas,nbas)
integer             :: nval, nvirt
real(realk),pointer :: S(:)
type(matrix)        :: U,VT,TmpT,CB

 nvirt = nbas - nocc
 nval  = nocc - ncore

!core block
if (ncore.gt.0) then

   call mem_alloc(S,ncore)
   call mat_init(U,ncore,ncore)
   call mat_init(VT,ncore,ncore)
   call mat_init(CB,ncore,ncore)
   
   ! CB = CMO(1:ncore,1:ncore)
   call mat_section(CMO,1,ncore,1,ncore,CB)
   call mat_dgesvd(CB,U,VT,ncore,S) !SVD of CB = CMO(1:ncore,1:ncore)
   call mem_dealloc(S)
   !Use CB as Tmporary F = VT^T*U^T
   call mat_dgemm('T','T',ncore,ncore,ncore,1E0_realk,VT,ncore,U,ncore,0E0_realk,CB,ncore)
   
   call mat_free(U)
   call mat_free(VT)
   
   !T(nbas,ncore) = sum_i^ncore CMO(1:nbas,i) * F(i,ncore)  
   call mat_init(TmpT,nbas,ncore)
   call mat_dgemm('N','N',nbas,ncore,ncore,1E0_realk,CMO,nbas,CB,ncore,0E0_realk,TmpT,nbas)
   call mat_insert_section(TmpT,1,nbas,1,ncore,T)
   call mat_free(CB)
   call mat_free(TmpT)
endif

!valence block

call mem_alloc(S,nval)
call mat_init(U,nval,nval)
call mat_init(VT,nval,nval)
call mat_init(CB,nval,nval)
!CB = CMO(ncore+1:ncore+nval,ncore+1:ncore+nval)
call mat_section(CMO,ncore+1,ncore+nval,ncore+1,ncore+nval,CB)
call mat_dgesvd(CB,U,VT,nval,S) !SVD of CB = CMO(ncore+1:ncore+nval,ncore+1:ncore+nval)
call mem_dealloc(S)
!Use CB as Tmporary F = VT^T*U^T
call mat_dgemm('T','T',nval,nval,nval,1E0_realk,VT,nval,U,nval,0E0_realk,CB,nval)
call mat_free(U)
call mat_free(VT)
call mat_init(U,nbas,nval)

!U = CMO(1:nbas,ncore+1:ncore+nval)
call mat_section(CMO,1,nbas,ncore+1,ncore+nval,U)
!T(1:nbas,ncore+1:ncore+nval) = CMO(1:nbas,ncore+1:ncore+nval) * F(1:nval,1:nval)
call mat_init(TmpT,nbas,nval)
call mat_dgemm('N','N',nbas,nval,nval,1E0_realk,U,nbas,CB,nval,0E0_realk,TmpT,nbas)
call mat_insert_section(TmpT,1,nbas,ncore+1,ncore+nval,T)
call mat_free(U)
call mat_free(CB)
call mat_free(TmpT)

!virtual block
if (nvirt .gt. 0) then
 
   call mem_alloc(S,nvirt)
   call mat_init(U,nvirt,nvirt)
   call mat_init(VT,nvirt,nvirt)
   call mat_init(CB,nvirt,nvirt)

   !CB = CMO(nocc+1:nbas,nocc+1:nbas)
   call mat_section(CMO,nocc+1,nbas,nocc+1,nbas,CB)
   call mat_dgesvd(CB,U,VT,nvirt,S) !SVD of CB = CMO(nocc+1:nbas,nocc+1:nbas)
   call mem_dealloc(S)
   !Use CB as Tmporary F = VT^T*U^T
   call mat_dgemm('T','T',nvirt,nvirt,nvirt,1E0_realk,VT,nvirt,U,nvirt,0E0_realk,CB,nvirt)
   call mat_free(U)
   call mat_free(VT)
   call mat_init(U,nbas,nvirt)

   !U = CMO(1:nbas,nocc+1:nocc+nvirt)
   call mat_section(CMO,1,nbas,nocc+1,nocc+nvirt,U)

   !T(1:nbas,nocc+1:nocc+nvirt) = CMO(1:nbas,nocc+1:nocc+nvirt) * F(1:nvirt,1:nvirt)
   call mat_init(TmpT,nbas,nvirt)

   call mat_dgemm('N','N',nbas,nvirt,nvirt,1E0_realk,U,nbas,CB,nvirt,0E0_realk,TmpT,nbas)
   call mat_insert_section(TmpT,1,nbas,nocc+1,nocc+nvirt,T)
   call mat_free(U)
   call mat_free(CB)
   call mat_free(TmpT)
end if
end subroutine leastchange_blocktransf

subroutine leastchange_orbspread(spread,uindex,n,&
                     &T,DIPX,DIPY,DIPZ,SECX,SECY,SECZ)
implicit none
integer :: n, uindex(2*n)
real(realk) :: spread(n)
type(matrix) :: T,DIPX,DIPY,DIPZ
type(matrix) :: SECX,SECY,SECZ
type(matrix) :: tmpT,tmp
real(realk) :: sec,dip
integer     :: i, itmp
real(realk), external :: ddot


call leastchange_iuindex(uindex(n+1),uindex,n)

call mat_init(tmpT,n,n)
call leastchange_rowreorder(tmpT,T,uindex(n+1),n)

call mat_assign(T,TmpT)
call mat_free(TmpT)

call mat_init(tmp,n,1)
call mat_init(tmpT,n,1)

 do i=1,n
!    call dsymv('u',n,1E0_realk,SECX,n,T(1,i),1,0E0_realk,tmp,1)
!     sec = ddot(n,T(1,i),1,tmp,1)
!     call dsymv('u',n,1E0_realk,DIPX,n,T(1,i),1,0E0_realk,tmp,1)
!     dip = ddot(n,T(1,i),1,tmp,1)
!     spread(i) = sec - dip*dip

    !TmpT(1:n) = T(1:n,i:i)
    call mat_section(TmpT,1,n,i,i,T)

    !tmp(1:n) = SECX(1:n,1:n)*T(1:n,i) 
    call mat_mul(SECX,TmpT,'N','N',1.0E0_realk,0.0E0_realk,tmp)
    sec = mat_dotproduct(TmpT,tmp)
    !tmp(1:n) = DIPX(1:n,1:n)*T(1:n,i) 
    call mat_mul(DIPX,TmpT,'N','N',1.0E0_realk,0.0E0_realk,tmp)
    dip = mat_dotproduct(TmpT,tmp)
    spread(i) = sec - dip*dip

    !tmp(1:n) = SECY(1:n,1:n)*T(1:n,i) 
    call mat_mul(SECY,TmpT,'N','N',1.0E0_realk,0.0E0_realk,tmp)
    sec = mat_dotproduct(TmpT,tmp)
    !tmp(1:n) = DIPY(1:n,1:n)*T(1:n,i) 
    call mat_mul(DIPY,TmpT,'N','N',1.0E0_realk,0.0E0_realk,tmp)
    dip = mat_dotproduct(TmpT,tmp)
    spread(i) = spread(i) + sec - dip*dip

    !tmp(1:n) = SECZ(1:n,1:n)*T(1:n,i) 
    call mat_mul(SECZ,TmpT,'N','N',1.0E0_realk,0.0E0_realk,tmp)
    sec = mat_dotproduct(TmpT,tmp)
    !tmp(1:n) = DIPZ(1:n,1:n)*T(1:n,i) 
    call mat_mul(DIPZ,TmpT,'N','N',1.0E0_realk,0.0E0_realk,tmp)
    dip = mat_dotproduct(TmpT,tmp)
    spread(i) = spread(i) + sec - dip*dip

    spread(i) = sqrt(spread(i))
 enddo
 call mat_free(tmp)
 call mat_free(tmpT)

end subroutine leastchange_orbspread

function leastchange_idmin(n,vec)
integer :: leastchange_idmin, n, i
real(realk) :: vec(n)
  
   leastchange_idmin=1
   do i=2,n
       if ((vec(leastchange_idmin)-vec(i))>1E-6_realk) leastchange_idmin=i
   enddo

return
end function leastchange_idmin


subroutine leastchange_rowreorder(B,A,uindex,n)
implicit none
integer :: n, i
integer :: uindex(n)
type(matrix),intent(in) :: A
type(matrix),intent(inout) :: B
!a row
type(matrix) :: TMP
!real(realk) :: A(n,n), B(n,n)

call mat_init(TMP,1,n)
call mat_assign(B,A)
do i=1,n
   IF(uindex(i).NE.i)THEN 
!      call dcopy(n,A(uindex(i),1),n,B(i,1),n)

      ! TMP= A(uindex(i):uindex(i),1:n)
      call mat_section(A,uindex(i),uindex(i),1,n,TMP)
      ! B(i:i,1:n) 
      call mat_insert_section(TMP,i,i,1,n,B)
   ENDIF
enddo
call mat_free(TMP)

end subroutine leastchange_rowreorder

subroutine leastchange_iuindex(iuindex,uindex,n)
integer :: n, i
integer :: iuindex(n),uindex(n)  
do i=1,n
   iuindex(uindex(i))=i
enddo
end subroutine leastchange_iuindex   

subroutine leastchange_rowsort_occup(uindex,CMO,ncore,nocc,n)
implicit none
integer :: n,ncore,nocc, uindex(n)
type(matrix) :: CMO !CMO(n,n)
real(realk),pointer :: occnum(:)
!real(realk), external :: ddot
integer :: i,j,tmp
real(realk) :: rtmp
type(matrix) :: Dens

! 1. Order orbitals according to occupied/virtual occupation
! --> "occupied" OAOs before "virtual" OAOs
! Build occnum(j) = sum_i^nocc CMO(j,i)*CMO(j,i)

call mat_init(Dens,n,n)
!Dens(1:n,1:n) = CMO(1:n,1:nocc)*CMO^T(1:nocc,1:n)
call mat_dgemm('N','T',n,n,nocc,1.0E0_realk,CMO,CMO%nrow,CMO,CMO%nrow,0.0E0_realk,Dens,Dens%nrow)

call mem_alloc(occnum,n)
!occnum(i) = Dens(i,i)
call mat_extract_diagonal(occnum,Dens)

call leastchange_SwapRows(nocc,n,n,occnum,CMO,uindex)

! 2. Order orbitals according to core/valence occupation
! --> "core" OAOs before "valence" OAOs
! Build occnum(j) = sum_i^ncore CMO(j,i)*CMO(j,i)

!Dens(1:n,1:n) = CMO(1:n,1:ncore)*CMO^T(1:ncore,1:n)
call mat_dgemm('N','T',n,n,ncore,1.0E0_realk,CMO,CMO%nrow,CMO,CMO%nrow,0.0E0_realk,Dens,Dens%nrow)

call mat_extract_diagonal(occnum,Dens)
call mat_free(Dens)
 
call leastchange_SwapRows(ncore,n,nocc,occnum,CMO,uindex)
call mem_dealloc(occnum)
 
end subroutine leastchange_rowsort_occup

subroutine leastchange_SwapRows(nocc,n,n2,occnum,CMO,uindex)
  implicit none
  integer,intent(in) :: nocc,n,n2
  type(matrix),intent(inout) :: CMO !CMO(n,n)
  integer,intent(inout) :: uindex(n)
  real(realk),intent(inout) :: occnum(n)
  !local variables
  integer :: i,j,tmp
  real(realk) :: rtmp
  integer, external     :: idamax
!FIXME: 
!1. First determine the swaps - reducing the number of swaps 
!                               if a row is swapped several times 
!2. Then do the actual swaps!!!
  do i=1,nocc
     j=idamax(n2-i+1,occnum(i),1)
     j=j+i-1
     if (j.gt.i) then
        call mat_swaprow(CMO,j,i)
        tmp=uindex(i); uindex(i)=uindex(j); uindex(j)=tmp
        rtmp=occnum(i); occnum(i)=occnum(j); occnum(j)=rtmp
     endif
  enddo
end subroutine leastchange_SwapRows

subroutine leastchange_rowsort_bf(decomp,uindex,T,CMO,ncore,nocc,n,ls,&
                                 &lupri,luerr)
implicit none
TYPE(lsitem) :: ls
type(decompItem),intent(inout) :: decomp
type(matrix) :: T, CMO
real(realk) :: spread(n)
real(realk),pointer :: mx(:)
type(matrix) :: DIPX, DIPY, DIPZ
type(matrix) :: SECX, SECY, SECZ
integer :: uindex(2*n), tmp, changes, ncore,nocc,nvirt,n,idx
integer, external :: idamax
real(realk),external :: ddot
integer :: i,j,lupri,luerr,iter
data iter /0/

call leastchange_propint(decomp,ls,lupri,luerr,DIPX,DIPY,DIPZ,SECX,SECY,SECZ,n)

nvirt = n - nocc

call mem_alloc(mx,nvirt+1)

do 
   
   changes=0
     
   do i=ncore+1,nocc
        
      !$OMP PARALLEL DO DEFAULT(SHARED)  PRIVATE(j,tmp,spread,T) &
      !$OMP FIRSTPRIVATE(CMO,uindex) SCHEDULE(DYNAMIC,1) 
      do j=nocc,n
         
         if (j.gt.nocc) then
            call mat_swaprow(CMO,j,i)
            tmp=uindex(i); uindex(i)=uindex(j); uindex(j)=tmp
         endif
         call leastchange_blocktransf(T,CMO,n,nocc,ncore)
         call leastchange_orbspread(spread,uindex,n,T,&
              & DIPX,DIPY,DIPZ,SECX,SECY,SECZ)
         
         if (j.gt.nocc) then
            call mat_swaprow(CMO,j,i)
            tmp=uindex(i); uindex(i)=uindex(j); uindex(j)=tmp
         endif
         
         mx(j-nocc+1) = spread(idamax(n,spread,1))
         
      enddo
      !$OMP END PARALLEL DO
      
      idx=leastchange_idmin(nvirt+1,mx) 
      j= idx + nocc - 1
      
      if (j.gt.nocc) then
         call mat_swaprow(CMO,j,i)
         tmp=uindex(i); uindex(i)=uindex(j); uindex(j)=tmp
         changes = changes+1
      endif
      
   enddo
     
   iter = iter + 1
   write(*,'(2X,I4,1X,A,I4,1X,A,F8.5)') &
        &     iter, 'Changes= ', changes, 'Max spread= ', mx(idx)
   
   if (changes.eq. 0) exit
   
enddo
  
call mem_dealloc(mx)
call mat_free(DIPX)
call mat_free(DIPY)
call mat_free(DIPZ)
call mat_free(SECX)
call mat_free(SECY)
call mat_free(SECZ)
end subroutine leastchange_rowsort_bf

end module leastchange_module

subroutine leastchangeOrbspreadStandalone(mx,ls,CMO,lupri,luerr)
use precision
use TYPEDEFTYPE, only: lsitem
use IntegralInterfaceMOD
use matrix_operations
use matrix_operations_aux
use matrix_module
use memory_handling
implicit none
real(realk)              :: mx
TYPE(lsitem)             :: ls
type(Matrix)             :: CMO
integer,intent(in)       :: lupri, luerr

integer                  :: n, i, nel, nocc,nvirt,occidx,virtidx
integer, parameter       :: nderiv=2, nMAT=10
type(Matrix)             :: propint(nMAT), PROPT,Tmp
real(realk)              :: sec, maxocc, maxvirt
real(realk), pointer :: spread(:),dip(:)
real(realk), external    :: ddot
integer, external        :: idamax

n=CMO%nrow
I = 5
call mat_init(propint(I),n,n)
call II_get_single_carmom(lupri,luerr,ls%setting,propint(I),I,nderiv,0E0_realk,0E0_realk,0E0_realk)
I = 8
call mat_init(propint(I),n,n)
call II_get_single_carmom(lupri,luerr,ls%setting,propint(I),I,nderiv,0E0_realk,0E0_realk,0E0_realk)
! propint(5)   -> SECX+SECY+SECZ 
call mat_daxpy(1E0_realk,propint(8),propint(5))
call mat_free(propint(8))
I = 10
call mat_init(propint(I),n,n)
call II_get_single_carmom(lupri,luerr,ls%setting,propint(I),I,nderiv,0E0_realk,0E0_realk,0E0_realk)
! propint(5)   -> SECX+SECY+SECZ 
call mat_daxpy(1E0_realk,propint(10),propint(5))
call mat_free(propint(10))

! SEC, prepare T and PROPT=SEC*T 
call mat_init(PROPT,n,n)
call mat_mul(propint(5),CMO,'n','n',1E0_realk,0E0_realk,PROPT)
call mat_free(propint(5))

call mat_init(Tmp,n,n)
!Tmp(1:n,1:n) = CMO^T(1:n,1:n)*PROP(1:n,1:n)
call mat_dgemm('T','N',n,n,n,1.0E0_realk,CMO,n,PROPT,PROPT%nrow,0.0E0_realk,Tmp,Tmp%nrow)
!spread(i) = Tmp(i,i) = CMO(i,1:n)^T*PROP(1:n,i)
call mem_alloc(spread,n)
call mat_extract_diagonal(spread,Tmp)

!DIPX

I = 2
call mat_init(propint(I),n,n)
call II_get_single_carmom(lupri,luerr,ls%setting,propint(I),I,nderiv,0E0_realk,0E0_realk,0E0_realk)
call mat_mul(propint(2),CMO,'n','n',1E0_realk,0E0_realk,PROPT)
call mat_free(propint(2))
!Tmp(1:n,1:n) = CMO^T(1:n,1:n)*PROP(1:n,1:n)
call mat_dgemm('T','N',n,n,n,1.0E0_realk,CMO,n,PROPT,PROPT%nrow,0.0E0_realk,Tmp,Tmp%nrow)
!dip(i) = Tmp(i,i) = T(i,1:n)^T*PROP(1:n,i)
call mem_alloc(dip,n)
call mat_extract_diagonal(dip,Tmp)
do i=1,n
   spread(i) = spread(i) - dip(i)*dip(i)
enddo

!DIPY
I = 3
call mat_init(propint(I),n,n)
call II_get_single_carmom(lupri,luerr,ls%setting,propint(I),I,nderiv,0E0_realk,0E0_realk,0E0_realk)
call mat_mul(propint(3),CMO,'n','n',1E0_realk,0E0_realk,PROPT)
call mat_free(propint(3))
!Tmp(1:n,1:n) = CMO^T(1:n,1:n)*PROP(1:n,1:n)
call mat_dgemm('T','N',n,n,n,1.0E0_realk,CMO,n,PROPT,PROPT%nrow,0.0E0_realk,Tmp,Tmp%nrow)
!dip(i) = Tmp(i,i) = T(i,1:n)^T*PROP(1:n,i)
call mat_extract_diagonal(dip,Tmp)
do i=1,n
   spread(i) = spread(i) - dip(i)*dip(i)
enddo

!DIPZ
I = 4
call mat_init(propint(I),n,n)
call II_get_single_carmom(lupri,luerr,ls%setting,propint(I),I,nderiv,0E0_realk,0E0_realk,0E0_realk)
call mat_mul(propint(4),CMO,'n','n',1E0_realk,0E0_realk,PROPT)
call mat_free(propint(4))
!Tmp(1:n,1:n) = CMO^T(1:n,1:n)*PROP(1:n,1:n)
call mat_dgemm('T','N',n,n,n,1.0E0_realk,CMO,n,PROPT,PROPT%nrow,0.0E0_realk,Tmp,Tmp%nrow)
!dip(i) = Tmp(i,i) = CMO(i,1:n)^T*PROP(1:n,i)
call mat_extract_diagonal(dip,Tmp)
do i=1,n
   spread(i) = spread(i) - dip(i)*dip(i)
   spread(i) = sqrt(spread(i))
enddo

call mem_dealloc(dip)
call mat_free(PROPT)
call mat_free(Tmp)

mx = spread(idamax(n,spread,1))

! Number of electrons
nel = ls%input%molecule%nelectrons
! number of occupied and virtual orbitals (assuming even number of electrons)
nocc = nel/2
nvirt = n-nocc
! Indices for orbitals with max orb. spread
occidx = idamax(nocc,spread(1:nocc),1)
virtidx = nocc + idamax(nvirt,spread(nocc+1:n),1)
! Max occ and virt orbital spreads
maxocc = spread(occidx)
maxvirt = spread(virtidx)

write(lupri,*)
write(lupri,'(4X,a,i8)') 'Number of electrons  in molecule = ', nel
write(lupri,'(4X,a,i8)') 'Number of occ. orb.  in molecule = ', nocc
write(lupri,'(4X,a,i8)') 'Number of virt. orb. in molecule = ', nvirt
write(lupri,*)
write(lupri,*)

!write(lupri,*) 'Orbspreads for occupied orbitals:'
!write(lupri,*) '*********************************'
!do i=1,nocc
!   write(lupri,*) i, spread(i)
!end do

!write(lupri,*)
!write(lupri,*)
!write(lupri,*) 'Orbspreads for virtual orbitals:'
!write(lupri,*) '********************************'
!do i=nocc+1,n
!   write(lupri,*) i, spread(i)
!end do
!write(lupri,*)
!write(lupri,*)
!write(lupri,*) 'Maximum orb. spread (MOS) summary'
!write(lupri,*) '*********************************'
! write(lupri,'(4X,a)') 'OCCUPIED: (Orbital index, MOS) = ', occidx, maxocc
! write(lupri,'(4X,a)') 'VIRTUAL : (Orbital index, MOS) = ', virtidx, maxvirt
! write(lupri,*)
! write(lupri,*)

call mem_dealloc(spread)

end subroutine leastchangeOrbspreadStandalone



subroutine leastchange_lcv(decomp,CMO,nocc,ls)
use precision
use leastchange_module
use decompMod
use loc_utils
use matrix_module
use matrix_operations
use memory_handling
implicit none
type(decompItem) :: decomp
type(Matrix) :: CMO
type(lsitem) :: ls
type(Matrix) :: CMOf
integer :: n, nocc, ncore, i
integer, allocatable :: uindex(:)
type(Matrix) :: Tf

  !print*,'inside leastchange_lcv'
  !convert CMO to orthonormal basis
  n = CMO%nrow
  call mat_init(CMOf,n,n)
  call mat_mul(decomp%U,CMO,'n','n',1E0_realk,0E0_realk,CMOf)

  !get ncore
  ncore = count_ncore(ls)

  !sorting index
  allocate(uindex(2*n))
  do i=1,n
    uindex(i)=i
  enddo

  !occupation sorting
  call leastchange_rowsort_occup(uindex,CMOf,ncore,nocc,n)

  !brute force sorting
  call mat_init(Tf,n,n)
  if (decomp%cfg_lcvbf) then
     call leastchange_rowsort_bf(decomp,uindex,Tf,CMOf,ncore,nocc,n,ls,&
          &decomp%lupri,decomp%luerr)
  endif

  !lcv basis
  call leastchange_blocktransf(Tf,CMOf,n,nocc,ncore)

  !inverse sorting index
  call leastchange_iuindex(uindex(n+1),uindex,n)

  call mat_free(CMOf)

  call mat_init(CMOf,n,n)
  !reorder to original
  call leastchange_rowreorder(CMOf,Tf,uindex(n+1),n)

  !back to gcscf basis
  call mat_free(Tf)
  deallocate(uindex)
  call mat_mul(decomp%U_inv,CMOf,'n','n',1E0_realk,0E0_realk,CMO)
  call mat_free(CMOf)

end subroutine leastchange_lcv

subroutine leastchange_lcm(decomp,CMO,nocc,ls)
use leastchange_module
use decompMod
use loc_utils
use precision
use matrix_module
use matrix_operations
use memory_handling
implicit none
type(decompItem),intent(in) :: decomp
type(Matrix) :: CMO
type(lsitem) :: ls
integer :: n, nocc, ncore
type(Matrix) :: CMOf,Tf

!  write(decomp%lupri,*)'inside leastchange_lcm'
!  print*,'inside leastchange_lcm'
  !convert CMO to orthonormal basis
  n = CMO%nrow
  call mat_init(CMOf,n,n)
  call mat_mul(decomp%U,CMO,'n','n',1E0_realk,0E0_realk,CMOf)
  !get ncore
  ncore = count_ncore(ls)

  !lcm basis
  call mat_init(Tf,n,n)
  call leastchange_blocktransf(Tf,CMOf,n,nocc,ncore)
  call mat_free(CMOf)

  !back to gcscf basis 
  call mat_mul(decomp%U_inv,Tf,'n','n',1E0_realk,0E0_realk,CMO)  
  call mat_free(Tf)

end subroutine leastchange_lcm

!> \brief Get normalized projected atomic orbitals (PAOs),
!> where the occupied space have been projected out.
!> I.e. the nbasis PAOs is a redundant set of
!> local orbitals which span the virtual space.
!> Only used for testing.
!> \author Kasper Kristensen
!> \date November 2011
subroutine get_PAOs(Cmo,S,MyLsitem,lupri)
  use matrix_util
  use files
  use TYPEDEFTYPE, only: lsitem
  use matrix_module
  use precision
  use matrix_operations
  use memory_handling

  implicit none
  !> Optimized MO coeffiecient (both occupied and virtual)
  type(matrix), intent(in) :: Cmo
  !> Overlap matrix in AO basis
  type(matrix), intent(in) :: S
  !> LSDALTON info
  type(lsitem), intent(inout) :: MyLsitem
  !> File unit number for DALTON.OUT
  integer, intent(in) :: lupri
  integer :: nelectrons
  type(matrix) :: DS, UnitMatrix, Q, QtSQ,D
  type(matrix) :: PAOs, PAO_overlap,Cocc,Cvirt
  type(matrix) :: SPCocc, SPCvirt
  integer :: nbasis, i,j, idx, nstart, nend, funit, nocc,nunocc, nunocc_end,lun
  real(realk), pointer :: Normalize_coeff(:), occ_vector(:), unocc_vector(:)
  real(realk), pointer :: eival(:)
  real(realk) :: coeff, tmp, occ_sum, unocc_sum,mx
  logical :: file_exist,OnMaster
  OnMaster=.TRUE.

  ! Initialize stuff
  ! ****************

  ! Number of electrons (assuming neutral molecule)
  nelectrons=0
  do i=1,MyLsitem%setting%MOLECULE(1)%p%nAtoms
     IF(MyLsitem%setting%MOLECULE(1)%p%Atom(i)%phantom)CYCLE
     nelectrons = nelectrons + MyLsitem%setting%MOLECULE(1)%p%Atom(i)%Charge
  enddo
  nbasis = cmo%nrow
  nocc = nelectrons/2
  nunocc = nbasis-nocc
  call mat_init(DS,nbasis,nbasis)
  call mat_init(UnitMatrix,nbasis,nbasis)
  call mat_init(Q,nbasis,nbasis)


  ! Occupied orbitals
  ! *****************
  call mat_init(Cocc,nbasis,nocc)
  nstart=1
  nend = nbasis*nocc
  Cocc%elms(nstart:nend) = cmo%elms(nstart:nend)


  ! Unoccupied orbitals
  ! *******************
  call mat_init(Cvirt,nbasis,nunocc)
  nstart = nbasis*nocc + 1
  nend = nbasis*nbasis
  nunocc_end = nbasis*nunocc
  Cvirt%elms(1:nunocc_end) = Cmo%elms(nstart:nend)


  ! Get density matrix from MOs
  ! ***************************
  ! Density matrix D = Cocc*Cocc^T 
  call mat_init(D,nbasis,nbasis)
  call mat_mul(Cocc,Cocc, 'n', 't', 1.d0, 0.d0,D)




  ! Project out occupied part of atomic orbitals
  ! ********************************************

  ! DS
  call mat_mul(D,S, 'n', 'n', 1.d0, 0.d0,DS)

  ! Unit matrix
  call mat_zero(UnitMatrix)
  do i=1,nbasis
     ! idx is the position in the matrix vector containing diagonal entry (i,i)
     idx = nbasis*(i-1) + i
     UnitMatrix%elms(idx) = 1.d0
  end do

  ! Q = 1-DS
  call mat_add(1.d0,UnitMatrix,-1.d0,DS,Q)
  call mat_free(UnitMatrix)
  call mat_free(DS)
  call mat_free(D)

  ! Q now contains the (non-normalized) expansion coeffisions for the PAOs



  ! Normalize PAOs
  ! **************
  call mat_init(QtSQ,nbasis,nbasis)

  ! Calculate Q^T S Q
  call util_AO_to_MO_2(S,Q,S,QtSQ,.true.)

  call mem_alloc(Normalize_coeff,nbasis)

  do i=1,nbasis
     ! idx is the position in the matrix vector containing diagonal entry (i,i)
     idx = nbasis*(i-1) + i
     ! Normalize coeffient for PAO_mu: 1/sqrt[ (S_SDS)_{mu,mu} ]
     if(QtSQ%elms(idx) < 0.0) then
        call lsquit('get_PAOs: Squared normalization factor smaller than zero,&
             & something is wrong!',-1)
     end if
     Normalize_coeff(i) = 1.d0/sqrt(QtSQ%elms(idx))
  end do
  call mat_free(QtSQ)


  ! For all orbitals, multiply orbital coefficients by normalization coefficients
  call mat_init(PAOs,nbasis,nbasis)
  call mat_zero(PAOs)
  do i=1,nbasis
     coeff = Normalize_coeff(i)
     do j=1,nbasis
        nstart  = (i-1)*nbasis + 1
        nend = i*nbasis
        PAOs%elms(nstart:nend) = coeff*Q%elms(nstart:nend) 
     end do
  end do
  call mat_free(Q)


  ! PAOs now contains the normalized projected atomic orbital coefficients.
  lun=-1
  CALL LSOPEN(lun,'pao','replace','UNFORMATTED')
  call mat_write_to_disk(lun,PAOs,OnMaster)
  call LSclose(lun,'KEEP')


  ! *******************************************************************************
  !                                Check PAOs                                     *
  ! *******************************************************************************

  ! Overlap matrix: PAO^T S PAO
  ! ***************************
  call mat_init(PAO_overlap,nbasis,nbasis)
  call util_AO_to_MO_2(S,PAOs,S,PAO_overlap,.true.)



  ! PAO/occupied overlap
  ! ********************

  ! Construct PAO/occupied overlap matrix: SPCocc = PAO^T S Cocc_can
  call mat_init(SPCocc,nbasis,nocc)
  call util_AO_to_MO_different_trans(PAOs, S, Cocc, SPCocc)
  call mat_free(Cocc)

  ! Construct PAO/virtual overlap matrix: SPCvirt = PAO^T S Cvirt_can
  call mat_init(SPCvirt,nbasis,nunocc)
  call util_AO_to_MO_different_trans(PAOs, S, Cvirt, SPCvirt)
  call mat_free(Cvirt)



  ! Vectors containing PAOs projected against occ and virt spaces
  ! *************************************************************

  call mem_alloc(occ_vector,nbasis)
  call mem_alloc(unocc_vector,nbasis)
  occ_vector=0.d0
  unocc_vector=0.d0


  ! Occupied vector
  ! '''''''''''''''
  do i=1,nbasis
     do j=1,nocc
        idx = (j-1)*nbasis + i
        tmp = SPCocc%elms(idx)
        ! occ_vector(i) = sum_j | <PAO(i) | Cocc_can(j)> |^2
        occ_vector(i) = occ_vector(i) + tmp**2
     end do
  end do
  call mat_free(SPCocc)


  ! Virtual vector
  ! ''''''''''''''
  do i=1,nbasis
     do j=1,nunocc
        idx = (j-1)*nbasis + i
        tmp = SPCvirt%elms(idx)
        ! unocc_vector(i) = sum_j | <PAO(i) | Cvirt_can(j)> |^2
        unocc_vector(i) = unocc_vector(i) + tmp**2
     end do
  end do
  call mat_free(SPCvirt)


  ! Maximum orbital spreads
  ! ***********************

  ! Get maximum orbital spreads for PAOs
  ! (only the maximum number in MaxOrbSpreads makes sense here 
  !  because we consider only the virtual space)
  write(lupri,*) 
  write(lupri,*) 'Calculating orbital spreads for projected atomic orbitals'
  write(lupri,*) 'NOTE: The orb spreads printed below ALL refer to PAOs!'
  write(lupri,*) '      - - The occupied/virtual partitioning is meaningless for the redundant PAOs'
  write(lupri,*) 
  call leastchangeOrbspreadStandalone(mx,MyLSitem,PAOs,lupri,lupri)
  call mat_free(PAOs)



  write(lupri,*) 
  write(lupri,*) 
  write(lupri,*) 
  write(lupri,'(5X,a)') '****************************************************************'
  write(lupri,'(5X,a)') '*             Projected atomic orbitals check                  * '
  write(lupri,'(5X,a)') '****************************************************************'
  write(lupri,*) 
  write(lupri,*) 


  write(lupri,'(5X,a)') 'Checking that PAOS are normalized'
  write(lupri,'(5X,a)') '---------------------------------'

  write(lupri,'(9X,a)') 'Orbital         Norm'
  do i=1,nbasis
     idx = nbasis*(i-1) + i
     write(lupri,'(5X,i8,4X,g18.8)') i, PAO_overlap%elms(idx)
  end do


  write(lupri,*) 
  write(lupri,*) 
  write(lupri,'(5X,a)') 'Checking that PAOS span virtual space'
  write(lupri,'(5X,a)') '-------------------------------------'

  write(lupri,'(9X,a,6X,a,10X,a)') 'Orbital', 'Occ. proj.', 'Virt. proj.'
  occ_sum=0.d0
  unocc_sum=0.d0
  do i=1,nbasis
     write(lupri,'(5X,i8,3X,g18.8,3X,g18.8)') i, occ_vector(i), unocc_vector(i)
     occ_sum = occ_sum + occ_vector(i)
     unocc_sum = unocc_sum + unocc_vector(i)
  end do
  write(lupri,'(20X,a)') "'''''''''''''''''''''''''''''''"
  write(lupri,'(10X,a,2X,g18.8,3X,g18.8)') 'SUM:',occ_sum, unocc_sum




  ! Free stuff
  call mem_dealloc(Normalize_coeff)
  call mem_dealloc(occ_vector)
  call mem_dealloc(unocc_vector)
  call mat_free(PAO_overlap)

end subroutine get_PAOs
