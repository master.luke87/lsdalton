!> @file 
!> Contains DSM module.

!> Density Subspace minimization (DSM) module. JCP, 121, 16-27, 2004; JCP 123, 074103, 2005
module LINSCA_DSM
   private

CONTAINS                                      
   SUBROUTINE dsm_init
   END SUBROUTINE dsm_init

end module LINSCA_DSM


