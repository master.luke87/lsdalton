     THIS IS A DEBUG BUILD
  
     ******************************************************************    
     **********  LSDalton - An electronic structure program  **********    
     ******************************************************************    
  
  
    This is output from LSDalton 1.0
  
  
     IF RESULTS OBTAINED WITH THIS CODE ARE PUBLISHED,
     THE FOLLOWING PAPER SHOULD BE CITED:
     
     K. Aidas, C. Angeli, K. L. Bak, V. Bakken, R. Bast,
     L. Boman, O. Christiansen, R. Cimiraglia, S. Coriani,
     P. Dahle, E. K. Dalskov, U. Ekstroem, T. Enevoldsen,
     J. J. Eriksen, P. Ettenhuber, B. Fernandez,
     L. Ferrighi, H. Fliegl, L. Frediani, K. Hald,
     A. Halkier, C. Haettig, H. Heiberg,
     T. Helgaker, A. C. Hennum, H. Hettema,
     E. Hjertenaes, S. Hoest, I.-M. Hoeyvik,
     M. F. Iozzi, B. Jansik, H. J. Aa. Jensen,
     D. Jonsson, P. Joergensen, J. Kauczor,
     S. Kirpekar, T. Kjaergaard, W. Klopper,
     S. Knecht, R. Kobayashi, H. Koch, J. Kongsted,
     A. Krapp, K. Kristensen, A. Ligabue,
     O. B. Lutnaes, J. I. Melo, K. V. Mikkelsen, R. H. Myhre,
     C. Neiss, C. B. Nielsen, P. Norman,
     J. Olsen, J. M. H. Olsen, A. Osted,
     M. J. Packer, F. Pawlowski, T. B. Pedersen,
     P. F. Provasi, S. Reine, Z. Rinkevicius,
     T. A. Ruden, K. Ruud, V. Rybkin,
     P. Salek, C. C. M. Samson, A. Sanchez de Meras,
     T. Saue, S. P. A. Sauer, B. Schimmelpfennig,
     K. Sneskov, A. H. Steindal, K. O. Sylvester-Hvid,
     P. R. Taylor, A. M. Teale, E. I. Tellgren,
     D. P. Tew, A. J. Thorvaldsen, L. Thoegersen,
     O. Vahtras, M. A. Watson, D. J. D. Wilson,
     M. Ziolkowski, and H. AAgren,
     "The Dalton quantum chemistry program system",
     WIREs Comput. Mol. Sci. (doi: 10.1002/wcms.1172)
  
  
                                               
    LSDalton authors in alphabetical order (main contribution(s) in parenthesis)
    ----------------------------------------------------------------------------
    Vebjoern Bakken,        University of Oslo,        Norway   (Geometry optimizer)
    Radovan Bast,           UiT The Arctic University of Norway (CMake, Testing)
    Pablo Baudin,           Aarhus University,         Denmark  (DEC,CCSD)
    Sonia Coriani,          University of Trieste,     Italy    (Response)
    Patrick Ettenhuber,     Aarhus University,         Denmark  (CCSD)
    Janus Juul Eriksen,     Aarhus University,         Denmark  (CCSD(T), DEC)
    Trygve Helgaker,        University of Oslo,        Norway   (Supervision)
    Stinne Hoest,           Aarhus University,         Denmark  (SCF optimization)
    Ida-Marie Hoeyvik,      Aarhus University,         Denmark  (Orbital localization, SCF opt)
    Robert Izsak,           University of Oslo,        Norway   (ADMM)
    Branislav Jansik,       Aarhus University,         Denmark  (Trilevel, orbital localization)
    Poul Joergensen,        Aarhus University,         Denmark  (Supervision)
    Joanna Kauczor,         Aarhus University,         Denmark  (Response solver)
    Thomas Kjaergaard,      Aarhus University,         Denmark  (RSP, INT, DEC, SCF, Readin, MPI, MAT)
    Andreas Krapp,          University of Oslo,        Norway   (FMM, dispersion-corrected DFT)
    Kasper Kristensen,      Aarhus University,         Denmark  (Response, DEC)
    Patrick Merlot,         University of Oslo,        Norway   (ADMM)
    Cecilie Nygaard,        Aarhus University,         Denmark  (SOEO)
    Jeppe Olsen,            Aarhus University,         Denmark  (Supervision)
    Simen Reine,            University of Oslo,        Norway   (Integrals, geometry optimizer)
    Vladimir Rybkin,        University of Oslo,        Norway   (Geometry optimizer, dynamics)
    Pawel Salek,            KTH Stockholm,             Sweden   (FMM, DFT functionals)
    Andrew M. Teale,        University of Nottingham   England  (E-coefficients)
    Erik Tellgren,          University of Oslo,        Norway   (Density fitting, E-coefficients)
    Andreas J. Thorvaldsen, University of Tromsoe,     Norway   (Response)
    Lea Thoegersen,         Aarhus University,         Denmark  (SCF optimization)
    Mark Watson,            University of Oslo,        Norway   (FMM)
    Marcin Ziolkowski,      Aarhus University,         Denmark  (DEC)
  
  
     NOTE:
      
     This is an experimental code for the evaluation of molecular
     energies and properties using various electronic structure models.
     The authors accept no responsibility for the performance of the code or
     for the correctness of the results.
      
     The code (in whole or part) is provided under a licence and
     is not to be reproduced for further distribution without
     the written permission of the authors or their representatives.
      
     See the home page "http://daltonprogram.org"
     for further information.
  
  
     Who compiled             | pablo
     Host                     | pablo-AU
     System                   | Linux-3.13.0-37-generic
     CMake generator          | Unix Makefiles
     Processor                | x86_64
     64-bit integers          | OFF
     MPI                      | OFF
     Fortran compiler         | /usr/bin/gfortran
     Fortran compiler version | GNU Fortran (Ubuntu 4.8.4-2ubuntu1~14.04.3) 4.8.4
     C compiler               | /usr/bin/gcc
     C compiler version       | gcc (Ubuntu 4.8.4-2ubuntu1~14.04.3) 4.8.4
     C++ compiler             | /usr/bin/g++
     C++ compiler version     | g++ (Ubuntu 4.8.4-2ubuntu1~14.04.3) 4.8.4
     BLAS                     | /opt/intel/composer_xe_2015.2.164/mkl/lib/intel64/
                              | libmkl_gf_lp64.so;/opt/intel/composer_xe_2015.2.16
                              | 4/mkl/lib/intel64/libmkl_sequential.so;/opt/intel/
                              | composer_xe_2015.2.164/mkl/lib/intel64/libmkl_core
                              | .so;/usr/lib/x86_64-linux-gnu/libpthread.so;/usr/l
                              | ib/x86_64-linux-gnu/libm.so
     LAPACK                   | /opt/intel/composer_xe_2015.2.164/mkl/lib/intel64/
                              | libmkl_lapack95_lp64.a;/opt/intel/composer_xe_2015
                              | .2.164/mkl/lib/intel64/libmkl_gf_lp64.so
     Static linking           | OFF
     Last Git revision        | e4693edfcb96b22c8b29bcf665164371d65a2126
     Git branch               | pablo/lofex
     Configuration time       | 2016-07-28 11:15:04.645763
  

         Start simulation
     Date and time (Linux)  : Thu Jul 28 11:31:49 2016
     Host name              : pablo-AU                                
                      
    ---------------------------------------------------
             PRINTING THE MOLECULE.INP FILE 
    ---------------------------------------------------
                      
    BASIS                                   
    6-31G Aux=cc-pVDZ-RI                    
                                            
                                            
    Atomtypes=2 Nosymmetry Angstrom Charge=0                                                                                
    Charge=8 Atoms=1                                                                                                        
    O   -2.904830    1.255230    0.000000                                                                                   
    Charge=1 Atoms=2                                                                                                        
    H   -1.934830    1.255230    0.000000                                                                                   
    H   -3.228160    0.442870   -0.420050                                                                                   
                      
    ---------------------------------------------------
             PRINTING THE LSDALTON.INP FILE 
    ---------------------------------------------------
                      
    **WAVE FUNCTIONS
    .HF
    *DENSOPT
    .RH
    .DIIS
    .START
    ATOMS
    .LCM
    .MAXIT
    1000
    **CC
    .RICC2
    .CANONICAL
    .MEMORY
    1.0
    .CCTHR
    1.0e-6
    *CCRESPONSE
    .NEXCIT
    2
    .SPECTRUM
    *END OF INPUT
 
 
 WARNING This basis format violate the old dalton basis format
 Which consist of 1 Exponent F16.9 and up to 6 contraction
 coefficients on the first line followed by a up to 7 contraction coefficients
 on the following lines until the full number of contraction coefficients are given
 We will try to this basis set, but this code is not very well testet. TK
 WARNING This basis format violate the old dalton basis format
 Which consist of 1 Exponent F16.9 and up to 6 contraction
 coefficients on the first line followed by a up to 7 contraction coefficients
 on the following lines until the full number of contraction coefficients are given
 We will try to this basis set, but this code is not very well testet. TK
 WARNING This basis format violate the old dalton basis format
 Which consist of 1 Exponent F16.9 and up to 6 contraction
 coefficients on the first line followed by a up to 7 contraction coefficients
 on the following lines until the full number of contraction coefficients are given
 We will try to this basis set, but this code is not very well testet. TK
 WARNING This basis format violate the old dalton basis format
 Which consist of 1 Exponent F16.9 and up to 6 contraction
 coefficients on the first line followed by a up to 7 contraction coefficients
 on the following lines until the full number of contraction coefficients are given
 We will try to this basis set, but this code is not very well testet. TK
 WARNING This basis format violate the old dalton basis format
 Which consist of 1 Exponent F16.9 and up to 6 contraction
 coefficients on the first line followed by a up to 7 contraction coefficients
 on the following lines until the full number of contraction coefficients are given
 We will try to this basis set, but this code is not very well testet. TK
 WARNING This basis format violate the old dalton basis format
 Which consist of 1 Exponent F16.9 and up to 6 contraction
 coefficients on the first line followed by a up to 7 contraction coefficients
 on the following lines until the full number of contraction coefficients are given
 We will try to this basis set, but this code is not very well testet. TK
 WARNING This basis format violate the old dalton basis format
 Which consist of 1 Exponent F16.9 and up to 6 contraction
 coefficients on the first line followed by a up to 7 contraction coefficients
 on the following lines until the full number of contraction coefficients are given
 We will try to this basis set, but this code is not very well testet. TK
                      
    Atoms and basis sets
      Total number of atoms        :      3
      THE  REGULAR   is on R =   1
    ---------------------------------------------------------------------
      atom label  charge basisset                prim     cont   basis
    ---------------------------------------------------------------------
          1 O      8.000 6-31G                     22        9 [10s4p|3s2p]                                 
          2 H      1.000 6-31G                      4        2 [4s|2s]                                      
          3 H      1.000 6-31G                      4        2 [4s|2s]                                      
    ---------------------------------------------------------------------
    total         10                               30       13
    ---------------------------------------------------------------------
                      
                      
    Atoms and basis sets
      Total number of atoms        :      3
      THE  AUXILIARY is on R =   2
    ---------------------------------------------------------------------
      atom label  charge basisset                prim     cont   basis
    ---------------------------------------------------------------------
          1 O      8.000 cc-pVDZ-RI                56       56 [7s5p4d2f|7s5p4d2f]                          
          2 H      1.000 cc-pVDZ-RI                14       14 [3s2p1d|3s2p1d]                              
          3 H      1.000 cc-pVDZ-RI                14       14 [3s2p1d|3s2p1d]                              
    ---------------------------------------------------------------------
    total         10                               84       84
    ---------------------------------------------------------------------
                      
                      
    Basic Molecule/Basis information
    --------------------------------------------------------------------
      Molecular Charge                   :    0.0000
      Regular basisfunctions             :       13
      Auxiliary basisfunctions           :       84
      Primitive Regular basisfunctions   :       30
      Primitive Auxiliary basisfunctions :       84
    --------------------------------------------------------------------
                      
    Configuration:
    ==============
    This is a Single core calculation. (no OpenMP)
    This is a serial calculation (no MPI)

    Density subspace min. method    : DIIS                    
    Density optimization            : Diagonalization                    

    Maximum size of Fock/density queue in averaging:   10

    Convergence threshold for gradient        :   0.10E-03
     
    We perform the calculation in the Grand Canonical basis
    (see PCCP 2009, 11, 5805-5813)
    To use the standard input basis use .NOGCBASIS
 
    Due to the presence of the keyword (default for correlation)
    .NOGCINTEGRALTRANSFORM
    We transform the input basis to the Grand Canonical
    basis and perform integral evaluation using this basis
     
    The Overall Screening threshold is set to              :  1.0000E-08
    The Screening threshold used for Coulomb               :  1.0000E-10
    The Screening threshold used for Exchange              :  1.0000E-08
    The Screening threshold used for One-electron operators:  1.0000E-15
    The SCF Convergence Criteria is applied to the gradnorm in AO basis

    End of configuration!

 
    Matrix type: mtype_dense
 
    A set of atomic calculations are performed in order to construct
    the Grand Canonical Basis set (see PCCP 11, 5805-5813 (2009))
    as well as JCTC 5, 1027 (2009)
    This is done in order to use the TRILEVEL starting guess and 
    perform orbital localization
    This is Level 1 of the TRILEVEL starting guess and is performed per default.
    The use of the Grand Canonical Basis can be deactivated using .NOGCBASIS
    under the **GENERAL section. This is NOT recommended if you do TRILEVEL 
    or orbital localization.
 

    Level 1 atomic calculation on 6-31G Charge   8
    ================================================
  
    *********************************************************************************** ###
     it            E(SCF)          dE(SCF)    exit        alpha RHshift       RHinfo  AO gradient ###
    *********************************************************************************** ###
      1    -71.4946859143    0.00000000000    0.00      0.00000    0.00    0.0000000    4.531E+00 ###
      2    -73.8341599123   -2.33947399805    0.00      0.00000    0.00    0.0000000    2.663E+00 ###
      3    -74.2580432558   -0.42388334348   -1.00      0.00000    0.00    0.0000000    1.607E-01 ###
      4    -74.2598387062   -0.00179545041   -1.00      0.00000    0.00    0.0000000    3.161E-02 ###
      5    -74.2598920070   -0.00005330082   -1.00      0.00000    0.00    0.0000000    2.600E-03 ###
      6    -74.2598924085   -0.00000040150   -1.00      0.00000    0.00    0.0000000    1.479E-06 ###

    Level 1 atomic calculation on 6-31G Charge   1
    ================================================
  
    *********************************************************************************** ###
     it            E(SCF)          dE(SCF)    exit        alpha RHshift       RHinfo  AO gradient ###
    *********************************************************************************** ###
      1     -0.3410549973    0.00000000000    0.00      0.00000    0.00    0.0000000    1.982E-01 ###
      2     -0.3488197504   -0.00776475307    0.00      0.00000    0.00    0.0000000    1.270E-02 ###
      3     -0.3488518065   -0.00003205613   -1.00      0.00000    0.00    0.0000000    3.937E-05 ###
 
    Matrix type: mtype_dense

    First density: Atoms in molecule guess

    Iteration 0 energy:      -75.944138361131
 
    Preparing to do S^1/2 decomposition...
  
    Relative convergence threshold for solver:  1.00000000E-02
    SCF Convergence criteria for gradient norm:  9.99999975E-05
    *********************************************************************************** ###
     it            E(SCF)          dE(SCF)    exit        alpha RHshift       RHinfo  AO gradient ###
    *********************************************************************************** ###
      1    -75.9265805613    0.00000000000    0.00      0.00000    0.00    0.0000000    1.718E+00 ###
      2    -75.9685132390   -0.04193267771    0.00      0.00000    0.00    0.0000000    1.001E+00 ###
      3    -75.9843275074   -0.01581426836   -1.00      0.00000    0.00    0.0000000    6.887E-02 ###
      4    -75.9845163177   -0.00018881031   -1.00      0.00000    0.00    0.0000000    1.176E-02 ###
      5    -75.9845230822   -0.00000676444   -1.00      0.00000    0.00    0.0000000    1.694E-03 ###
      6    -75.9845231992   -0.00000011701   -1.00      0.00000    0.00    0.0000000    1.099E-04 ###
      7    -75.9845231996   -0.00000000046   -1.00      0.00000    0.00    0.0000000    1.980E-05 ###
    SCF converged in      7 iterations
    >>>  CPU Time used in SCF iterations is   0.36 seconds
    >>> wall Time used in SCF iterations is   0.36 seconds

    Total no. of matmuls in SCF optimization:         59

    Number of occupied orbitals:       5
    Number of virtual orbitals:        8

    Number of occupied orbital energies to be found:       1
    Number of virtual orbital energies to be found:        1


    Calculation of HOMO-LUMO gap
    ============================

    Calculation of occupied orbital energies converged in     5 iterations!

    Calculation of virtual orbital energies converged in     6 iterations!

     E(LUMO):                         0.201605 au
    -E(HOMO):                        -0.498125 au
    -------------------------------------------------
     HOMO-LUMO Gap (iteratively):     0.699730 au


    ********************************************************
     it       dE(HF)          exit   RHshift    RHinfo 
    ********************************************************
      1    0.00000000000    0.0000    0.0000    0.0000000
      2   -0.04193267771    0.0000    0.0000    0.0000000
      3   -0.01581426836   -1.0000    0.0000    0.0000000
      4   -0.00018881031   -1.0000    0.0000    0.0000000
      5   -0.00000676444   -1.0000    0.0000    0.0000000
      6   -0.00000011701   -1.0000    0.0000    0.0000000
      7   -0.00000000046   -1.0000    0.0000    0.0000000

    ======================================================================
                       LINSCF ITERATIONS:
      It.nr.               Energy                 AO Gradient norm
    ======================================================================
        1           -75.92658056132371768854      0.171788818626525D+01
        2           -75.96851323903459274334      0.100068010643628D+01
        3           -75.98432750739429764053      0.688657971245424D-01
        4           -75.98451631770838332613      0.117552000494058D-01
        5           -75.98452308215087214194      0.169385283069236D-02
        6           -75.98452319916466990435      0.109915107778845D-03
        7           -75.98452319962056833447      0.198017815317274D-04

          SCF converged !!!! 
             >>> Final SCF results from LSDALTON <<<


          Final HF energy:                       -75.984523199621
          Nuclear repulsion:                       9.062741299348
          Electronic energy:                     -85.047264498969

      %LOC%
      %LOC% Localized orbitals written to lcm_orbitals.u
      %LOC%

    Number of electrons  in molecule =       10
    Number of occ. orb.  in molecule =        5
    Number of virt. orb. in molecule =        8




    -- Full molecular info --

    FULL: Overall charge of molecule    :      0

    FULL: Number of electrons           :     10
    FULL: Number of atoms               :      3
    FULL: Number of basis func.         :     13
    FULL: Number of aux. basis func.    :     84
    FULL: Number of core orbitals       :      1
    FULL: Number of valence orbitals    :      4
    FULL: Number of occ. orbitals       :      5
    FULL: Number of occ. alpha orbitals :      0
    FULL: Number of occ. beta  orbitals :      0
    FULL: Number of virt. orbitals      :      8
    FULL: Local memory use type full    :  0.32E-05
    FULL: Distribute matrices           : F

 Allocate space for molecule%Co on Master use_bg= F
 Allocate space for molecule%Cv on Master use_bg= F
    Memory set in input to be:    1.000     GB


    =============================================================================
         -- Full molecular Coupled-Cluster calculation -- 
    =============================================================================

    Using canonical orbitals as requested in input!



 ================================================ 
              Full molecular driver               
 ================================================ 


--------------------------
  Coupled-cluster energy  
--------------------------

Wave function    = RICC2   
MaxIter          =  100
Num. b.f.        =   13
Num. occ. orb.   =    5
Num. unocc. orb. =    8
Convergence      =  0.1E-05
Debug mode       =    F
Print level      =    2
Use CROP         =    T
CROP subspace    =    3
Preconditioner   =    T
Precond. B       =    T
Singles          =    T

 ### Starting CC iterations
 ### ----------------------
 ###  Iteration     Residual norm          CC energy
 ###      1         0.241132043E-01       -0.129640034    
 ###      2         0.542716381E-02       -0.130254377    
 ###      3         0.602902729E-03       -0.130426380    
 ###      4         0.230796316E-03       -0.130435974    
 ###      5         0.225395500E-04       -0.130437835    
 ###      6         0.158944938E-05       -0.130437871    
 ###      7         0.127875556E-06       -0.130437870    
 CCSOL: MAIN LOOP      :  1.58    s


-------------------------------
  Coupled-cluster job summary  
-------------------------------

Hooray! CC equation is solved!
CCSOL: Total cpu time    =  0.988     s
CCSOL: Total wall time   =  0.988     s
Singles amplitudes norm  =  0.1960413E-01
Total amplitudes norm    =  0.1960413E-01
Corr. energy             =     -0.1304378696
Number of CC iterations  =    7

--------------------------------------------
  Coupled-cluster ground state multipliers  
--------------------------------------------

Wave function    = RICC2   
MaxIter          =  100
Num. b.f.        =   13
Num. occ. orb.   =    5
Num. unocc. orb. =    8
Convergence      =  0.1E-05
Debug mode       =    F
Print level      =    2
Use CROP         =    T
CROP subspace    =    3
Preconditioner   =    T
Precond. B       =    T
Singles          =    T

 ### Starting Lagrangian iterations
 ### ------------------------------
 ###  Iteration     Residual norm
 ###      1         0.105118265E-01
 ###      2         0.264275888E-02
 ###      3         0.458303628E-03
 ###      4         0.393554039E-04
 ###      5         0.375407934E-05
 ###      6         0.512226228E-06
 CCSOL: MAIN LOOP      :  2.68    s


-------------------------------
  Coupled-cluster job summary  
-------------------------------

Yeeehaw! left-transformations converged!
CCSOL: Total cpu time    =  0.903     s
CCSOL: Total wall time   =  0.905     s
Singles multiplier norm  =  0.3627532E-01
Total multiplier norm    =  0.3627532E-01
Number of CC iterations  =    6


 -------------------------------
  Non-linear CC Davidson solver 
 -------------------------------

 Self-consistency threshold =  0.100E-03
 Solve for right eigenvectors
 Input excitation energies:
      0.69973031
      0.75080533

 ### Starting Davidson Macro-iterations
 ### ----------------------------------
 ###
 ### State no.  Macro-it   # Micro-it  delta(freq.)     freq. (a.u.)
 ### ---------------------------------------------------------------
 ###     2          1            1     0.375526E+00      0.37527958
 ###     2          2            3     0.108164E-01      0.38609601
 ###     2          3            3     0.512822E-03      0.38558319
 ###     2          4            2     0.242113E-04      0.38560740
 ### ---------------------------------------------------------------
 ###     1          1            4     0.822363E-03      0.30217370
 ###     1          2            2     0.353199E-04      0.30220902


      ************************************************************
      *               RICC2    excitation energies               *
      ************************************************************

            Exci.    Hartree           eV            cm-1         
              1      0.3022090       8.223530       66327.21    
              2      0.3856074       10.49292       84631.04    


 -------------------------------
  Non-linear CC Davidson solver 
 -------------------------------

 Self-consistency threshold =  0.100E-03
 Solve for left eigenvectors
 Input excitation energies:
      0.30220902
      0.38560740

 ### Starting Davidson Macro-iterations
 ### ----------------------------------
 ###
 ### State no.  Macro-it   # Micro-it  delta(freq.)     freq. (a.u.)
 ### ---------------------------------------------------------------
 ###     2          1            4     0.110627E-05      0.38560630
 ### ---------------------------------------------------------------
 ###     1          1            3     0.150120E-05      0.30220751


      ************************************************************
      *               RICC2    excitation energies               *
      ************************************************************

            Exci.    Hartree           eV            cm-1         
              1      0.3022075       8.223489       66326.88    
              2      0.3856063       10.49289       84630.80    

------------------------------------------
  Coupled-cluster transition multipliers  
------------------------------------------

Wave function    = RICC2   
MaxIter          =  100
Num. b.f.        =   13
Num. occ. orb.   =    5
Num. unocc. orb. =    8
Convergence      =  0.1E-05
Debug mode       =    F
Print level      =    2
Use CROP         =    T
CROP subspace    =    3
Preconditioner   =    T
Precond. B       =    T
Singles          =    T

 ### Starting Lagrangian iterations
 ### ------------------------------
 ###  Iteration     Residual norm
 ###      1         0.721433550E-02
 ###      2         0.117350347E-02
 ###      3         0.389554633E-04
 ###      4         0.991024307E-06
 CCSOL: MAIN LOOP      :  8.89    s


-------------------------------
  Coupled-cluster job summary  
-------------------------------

Yeeehaw! left-transformations converged!
CCSOL: Total cpu time    =  0.618     s
CCSOL: Total wall time   =  0.618     s
Singles multiplier norm  =  0.4480721E-01
Total multiplier norm    =  0.4480721E-01
Number of CC iterations  =    4

------------------------------------------
  Coupled-cluster transition multipliers  
------------------------------------------

Wave function    = RICC2   
MaxIter          =  100
Num. b.f.        =   13
Num. occ. orb.   =    5
Num. unocc. orb. =    8
Convergence      =  0.1E-05
Debug mode       =    F
Print level      =    2
Use CROP         =    T
CROP subspace    =    3
Preconditioner   =    T
Precond. B       =    T
Singles          =    T

 ### Starting Lagrangian iterations
 ### ------------------------------
 ###  Iteration     Residual norm
 ###      1         0.441211946E-01
 ###      2         0.135536627E-01
 ###      3         0.450081340E-04
 ###      4         0.190699001E-05
 ###      5         0.106796104E-06
 CCSOL: MAIN LOOP      :  10.1    s


-------------------------------
  Coupled-cluster job summary  
-------------------------------

Yeeehaw! left-transformations converged!
CCSOL: Total cpu time    =  0.760     s
CCSOL: Total wall time   =  0.760     s
Singles multiplier norm  =  0.8432190E-01
Total multiplier norm    =  0.8432190E-01
Number of CC iterations  =    5


 RICC2    Right transition dipole moments (a.u.):
 ------------------------------------------------

 State            X               Y               Z             Freq.
    1       -0.94964352E-16 -0.82602899E-01  0.15975072      0.30220902    
    2       -0.26527741      0.33325537      0.17231759      0.38560740    


 RICC2    Left  transition dipole moments (a.u.):
 ------------------------------------------------

 State            X               Y               Z             Freq.
    1       -0.32424086E-14 -0.16069132      0.31077062      0.30220902    
    2       -0.51867674      0.65158803      0.33691904      0.38560740    


 RICC2    transition dipole strengths (a.u.):
 --------------------------------------------

 State            X               Y               Z             Freq.
    1        0.30791323E-30  0.13273569E-01  0.49645831E-01  0.30220902    
    2        0.13759322      0.21714521      0.58057078E-01  0.38560740    


 RICC2    Oscillator strengths:
 ==============================

 State        Osc. str.     Freq. (a.u.)         (eV)          (cm-1) 
    1        0.12676540E-01  0.30220902       8.2235301       66327.210    
    2        0.10611800      0.38560740       10.492917       84631.040    




    ******************************************************************************
    *                             CC ENERGY SUMMARY                              *
    ******************************************************************************

     E: Hartree-Fock energy                            :      -75.9845231996
     E: Correlation energy                             :       -0.1304378696
     E: Total RI-CC2 energy                            :      -76.1149610692



    CC Memory summary
    -----------------
     Allocated memory for array4   :   0.000     GB
     Memory in use for array4      :   0.000     GB
     Max memory in use for array4  :   0.000     GB
    ------------------


    ------------------------------------------------------
    Total CPU  time used in CC           :         10.2161     s
    Total Wall time used in CC           :         10.2120     s
    ------------------------------------------------------


    Hostname       : pablo-AU                                          
    Job finished   : Date: 28/07/2016   Time: 11:31:59



    =============================================================================
                              -- end of CC program --
    =============================================================================



    Total no. of matmuls used:                        96
    Total no. of Fock/KS matrix evaluations:           8
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
                      Memory statistics          
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
      Allocated memory (TOTAL):         0 byte Should be zero, otherwise a leakage is present
 
      Max allocated memory, TOTAL                          6.719 MB
      Max allocated memory, type(matrix)                  46.024 kB
      Max allocated memory, real(realk)                    6.616 MB
      Max allocated memory, integer                      103.508 kB
      Max allocated memory, logical                        1.092 kB
      Max allocated memory, character                      2.640 kB
      Max allocated memory, AOBATCH                       77.824 kB
      Max allocated memory, BATCHTOORB                     0.112 kB
      Max allocated memory, ARRAY                          4.992 kB
      Max allocated memory, ODBATCH                        3.520 kB
      Max allocated memory, LSAOTENSOR                     5.472 kB
      Max allocated memory, SLSAOTENSOR                    6.624 kB
      Max allocated memory, ATOMTYPEITEM                 229.584 kB
      Max allocated memory, ATOMITEM                       2.048 kB
      Max allocated memory, LSMATRIX                      11.536 kB
      Max allocated memory, OverlapT                      68.160 kB
      Max allocated memory, linkshell                      0.432 kB
      Max allocated memory, integrand                    716.800 kB
      Max allocated memory, integralitem                   1.280 MB
      Max allocated memory, IntWork                      161.608 kB
      Max allocated memory, Overlap                        4.308 MB
      Max allocated memory, ODitem                         2.560 kB
      Max allocated memory, LStensor                      65.328 kB
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

    Allocated MPI memory a cross all slaves:          0 byte  - Should be zero - otherwise a leakage is present
    This is a non MPI calculation so naturally no memory is allocated on slaves!
    >>>  CPU Time used in LSDALTON is  10.80 seconds
    >>> wall Time used in LSDALTON is  10.80 seconds

    End simulation
     Date and time (Linux)  : Thu Jul 28 11:31:59 2016
     Host name              : pablo-AU                                
