#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > LSDALTON_screen1.info <<'%EOF%'
   LSDALTON_screen1
   -------------
   Molecule:         Methane with -OH, and two Fs
   Wave Function:    HF
   Test Purpose:     check screening 
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > LSDALTON_screen1.mol <<'%EOF%'
ATOMBASIS
Methane with -OH,and two Fs
b3lyp/6-31+g(d,p) optimized geo.
Atomtypes=4 Nosymmetry
Charge=1. Atoms=2 Basis=6-31G
H    0.520821    0.080551    1.451675
H    1.965224   -1.097271    0.053015
Charge=6.    Atoms=1  Basis=6-31G
C    0.430423    0.005605    0.364687
Charge=8.    Atoms=1  Basis=3-21G
O    1.023374   -1.101579   -0.177807
Charge=9.    Atoms=2  Basis=3-21G
F    0.989971    1.157250   -0.160167
F   -1.303842   -0.036443   -0.048756
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > LSDALTON_screen1.dal <<'%EOF%'
**GENERAL
.NOGCBASIS
**INTEGRALS
.DEBUGSCREEN
**WAVE FUNCTIONS
.HF
*DENSOPT
.ARH
.START
H1DIAG
**INFO
.DEBUG_MPI_MEM
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >LSDALTON_screen1.check
cat >> LSDALTON_screen1.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# ENERGY test
CRIT1=`$GREP "Final HF energy:    * \-305.5370962" $log | wc -l`
TEST[1]=`expr  $CRIT1`
CTRL[1]=1
ERROR[1]="HF energy not correct"

# ENERGY test
CRIT1=`$GREP "di_screen_test SUCCESSFUL" $log | wc -l`
TEST[2]=`expr  $CRIT1`
CTRL[2]=1
ERROR[2]="screening not correct"

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[3]=`expr  $CRIT1`
CTRL[3]=1
ERROR[3]="Memory leak -"

# MPI Memory test
CRIT1=`$GREP "[0-9][0-9] byte  \- Should be zero \- otherwise a leakage is present" $log | wc -l`
TEST[4]=`expr  $CRIT1`
CTRL[4]=0
ERROR[4]="MPI Memory leak -"

PASSED=1
for i in 1 2 3 4
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo TEST ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
