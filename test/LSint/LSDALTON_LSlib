#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > LSDALTON_LSlib.info <<'%EOF%'
   LSDALTON_LSlib
   -------------
   Molecule:         HCN/cc-pVDZ
   Wave Function:    HF
   Test Purpose:     Check LSlib library fnctions. TK
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > LSDALTON_LSlib.mol <<'%EOF%'
BASIS
cc-pVDZ Aux=cc-pVDZ-uncontracted
1 HCN molecules placed 20 atomic units apart
STRUCTURE IS NOT OPTIMIZED. STO-2G basis 
Atomtypes=3 Nosymmetry
Charge=1. Atoms=1
H   0.0   0.0    -1.0 
Charge=7. Atoms=1
N   0.0   0.0     1.5
Charge=6. Atoms=1
C   0.0   0.0     0.0
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > LSDALTON_LSlib.dal <<'%EOF%'
**GENERAL
.NOGCBASIS
**INTEGRALS
.DEBUGLSLIB
**WAVE FUNCTIONS
.HF
*DENSOPT
.RH
.DIIS
.START
H1DIAG
.MAXIT
1
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >LSDALTON_LSlib.check
cat >> LSDALTON_LSlib.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi
CRIT1=`$GREP "LSlib Test Successful" $log | wc -l`
TEST[1]=`expr  $CRIT1`
CTRL[1]=1
ERROR[1]="II_test_LSlib not successful -"

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[2]=`expr  $CRIT1`
CTRL[2]=1
ERROR[2]="Memory leak -"

# MPI Memory test
CRIT1=`$GREP "[0-9][0-9] byte  \- Should be zero \- otherwise a leakage is present" $log | wc -l`
TEST[3]=`expr  $CRIT1`
CTRL[3]=0
ERROR[3]="MPI Memory leak -"

PASSED=1
for i in 1 2 3
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo TEST ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
