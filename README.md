# Quick start

Clone the repository:
```
$ git clone --recursive git@gitlab.com:dalton/lsdalton.git
```

Build the code:
```
$ cd lsdalton
$ ./setup [--help]
$ cd build
$ make -j
```

Run the test set:
```
$ ctest [-j4]
```

Dalton Developer’s Guide: http://dalton-devguide.readthedocs.org


## Building the release tarball

```
$ wget https://raw.githubusercontent.com/meitar/git-archive-all.sh/master/git-archive-all.sh
$ bash git-archive-all.sh
```
